define("AccountPageV2", [], function() {
	return {
		entitySchemaName: "Account",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{
			"Chart00bd2b7a-d73a-4b01-81f3-655ed8d4e9b8": {
				"moduleId": "Chart00bd2b7a-d73a-4b01-81f3-655ed8d4e9b8",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Chart00bd2b7a-d73a-4b01-81f3-655ed8d4e9b8",
							"recordId": "e4143cf6-1729-4c7a-ae6d-6943f61e626d",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"SuPlanningTaba9086e2c-4766-4e48-bd3a-7938c6381bcd": {
				"moduleId": "SuPlanningTaba9086e2c-4766-4e48-bd3a-7938c6381bcd",
				"moduleName": "SumPlanningModule",
				"config": {
					"profileCode": "SumTradeMarketingPageV2",
					"schemaName": "SumPlanningTab",
					"parameters": {
						"viewModelConfig": {
							"UsePageHeader": false
						}
					},
					"record": {
						"schemaName": "SumTradeMarketing",
						"id": {
							"attributeValue": "PrimaryColumnValue"
						}
					},
					"planningId": "aa534a92-e6a5-45fb-ba17-dbd738f110a1",
					"planningFilters": {
						"startDate": {
							"attributeValue": "SumStartDate"
						},
						"endDate": {
							"attributeValue": "SumDueDate"
						},
						"filters": [
							{
								"levelId": "6df1abd2-12cd-4946-93ad-579998770bde",
								"levelColumn": "Id",
								"levelColumnCaption": "Id",
								"value": {
									"attributeValue": "SumAccount"
								}
							},
							{
								"levelId": "957ade36-a983-4d52-8cb0-f2e4c50521e9",
								"levelColumn": "[SumTMActivityProducts:SumProduct].SumTMActivity.Id",
								"levelColumnCaption": "Продукты ТМ Активности (по колонке Продукт).ТМ активность",
								"value": {
									"attributeValue": "Id"
								}
							}
						]
					}
				}
			}
		}/**SCHEMA_MODULES*/ ,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/ ,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/ ,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		rules: {},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "SuComponent1Tab",
				"values": {
					"caption": "Component 1",
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "SuComponent1ModuleContainer",
				"values": {
					"id": "SuComponent1ModuleContainer",
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"items": []
				},
				"parentName": "SuComponent1Tab",
				"propertyName": "items"
			},
			{
				"operation": "insert",
				"name": "SuComponent1Module",
				"parentName": "SuComponent1ModuleContainer",
				"propertyName": "items",
				"values": {
					"itemType": Terrasoft.ViewItemType.MODULE,
					"moduleName": "SuComponent1Module",
					"instanceConfig": {},
					"makeUniqueId": true
				}
			},
			{
				"operation": "insert",
				"name": "SuComponent2Tab",
				"values": {
					"caption": "Component 2",
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "SuComponent2ModuleContainer",
				"values": {
					"id": "SuComponent2ModuleContainer",
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"items": []
				},
				"parentName": "SuComponent2Tab",
				"propertyName": "items"
			},
			{
				"operation": "insert",
				"name": "SuComponent2Module",
				"parentName": "SuComponent2ModuleContainer",
				"propertyName": "items",
				"values": {
					"itemType": Terrasoft.ViewItemType.MODULE,
					"moduleName": "SuComponent2Module",
					"instanceConfig": {},
					"makeUniqueId": true
				}
			},
		]/**SCHEMA_DIFF*/
	};
});
