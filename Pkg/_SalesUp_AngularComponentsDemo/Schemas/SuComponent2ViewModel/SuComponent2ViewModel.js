define("SuComponent2ViewModel", [], function() {
	 
	Ext.define("Terrasoft.configuration.SuComponent2ViewModel", {
		extend: "Terrasoft.BaseViewModel",
		alternateClassName: "Terrasoft.SuComponent2ViewModel",

		Ext: null,
		Terrasoft: null,
		sandbox: null,
		
	});

	return Terrasoft.SuComponent2ViewModel;
});