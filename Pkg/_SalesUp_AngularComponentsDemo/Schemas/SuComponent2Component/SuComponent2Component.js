define("SuComponent2Component",  ["SuComponent2", "css!SuComponent2Css", "css!SuComponent2Component"], function() {
	Ext.define("Terrasoft.control.SuComponent2Component", {
		extend: "Terrasoft.controls.Component",
		alternateClassName: "Terrasoft.SuComponent2Component",

		domAttributes: [],

		styles: {},
		parentComponent: Ext.emptyString,
		mixins: {},
        
		/**
		 * @inheritDoc Terrasoft.Component#tpl
		 * @override
		 */
		tpl: [
			/*jshint white:false */
			/*jshint quotmark:true */
			//jscs:disable
			'<div id=\"{id}-wrap\" style=\"{styles}\">',
            '<su-component-2 id=\"{id}\">',
			'</su-component-2>',
			'</div>',
			//jscs:enable
			/*jshint quotmark:false */
			/*jshint white:true */
        ],

		/**
		 * @inheritDoc Terrasoft.Component#getSelectors
		 * @override
		 */
		getSelectors: function() {
			return {
				wrapEl: "#" + this.id + "-wrap",
				suComponent2ComponentEl: "#" + this.id
			};
		},

		/**
		 * @inheritDoc Terrasoft.Component#getTplData
		 * @override
		 */
		getTplData: function() {
			const tplData = this.callParent(arguments);
			this.selectors = this.getSelectors();
			return tplData;
        },
        
	});

	return Terrasoft.SuComponent2Component;

});
