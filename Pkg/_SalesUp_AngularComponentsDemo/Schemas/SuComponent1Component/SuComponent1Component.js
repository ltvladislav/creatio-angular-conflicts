define("SuComponent1Component",  ["SuComponent1", "css!SuComponent1Css", "css!SuComponent1Component"], function() {
	Ext.define("Terrasoft.control.SuComponent1Component", {
		extend: "Terrasoft.controls.Component",
		alternateClassName: "Terrasoft.SuComponent1Component",

		domAttributes: [],

		styles: {},
		parentComponent: Ext.emptyString,
		mixins: {},
        
		/**
		 * @inheritDoc Terrasoft.Component#tpl
		 * @override
		 */
		tpl: [
			/*jshint white:false */
			/*jshint quotmark:true */
			//jscs:disable
			'<div id=\"{id}-wrap\" style=\"{styles}\">',
            '<su-component-1 id=\"{id}\">',
			'</su-component-1>',
			'</div>',
			//jscs:enable
			/*jshint quotmark:false */
			/*jshint white:true */
        ],

		/**
		 * @inheritDoc Terrasoft.Component#getSelectors
		 * @override
		 */
		getSelectors: function() {
			return {
				wrapEl: "#" + this.id + "-wrap",
				suComponent1ComponentEl: "#" + this.id
			};
		},

		/**
		 * @inheritDoc Terrasoft.Component#getTplData
		 * @override
		 */
		getTplData: function() {
			const tplData = this.callParent(arguments);
			this.selectors = this.getSelectors();
			return tplData;
        },
        
	});

	return Terrasoft.SuComponent1Component;

});
