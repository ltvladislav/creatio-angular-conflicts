define("SuComponent1Module", ["SuComponent1Component", "SuComponent1ViewModel"],
function() {
    Ext.define("Terrasoft.configuration.SuComponent1Module", {
        extend: "Terrasoft.configuration.BaseModule",
        alternateClassName: "Terrasoft.SuComponent1Module",

        Ext: null,
        sandbox: null,
        Terrasoft: null,

        view: null,
        model: null,

        /**
         * @inheritDoc Terrasoft.BaseModule#init
         * @override
         */
        init: async function() {
            this.model = Ext.create("Terrasoft.SuComponent1ViewModel", {
                sandbox: this.sandbox,
                Ext: this.Ext,
                Terrasoft: this.Terrasoft
            });
			this._bindViewToModel();
            this.callParent(arguments);
        },

        /**
         * @inheritDoc Terrasoft.BaseModule#render
         * @override
         */
        render: function(renderTo) {
            this.callParent(arguments);
            if (!renderTo.dom) {
                renderTo = Ext.get(renderTo.id);
            }
            this._renderAngularComponent(renderTo);
        },


        /**
         * Renders chat grid.
         * @param {Object} renderTo Element to rendering.
         * @private
         */
         _renderAngularComponent: function(renderTo) {
            this.view = Ext.create("Terrasoft.SuComponent1Component", {                
                parentComponent: renderTo.id
            });
			this._bindViewToModel();
            this.view.render(renderTo);
        },

        _bindViewToModel: function() {
			if (this.model && this.view) {
				this.view.bind(this.model);
			}
		},

        /**
         * @inheritDoc Terrasoft.BaseModule#destroy
         * @override
         */
        destroy: function() {
            if (this.view) {
                this.view.destroy();
            }
            if (this.model) {
                this.model.destroy();
            }
            this.callParent(arguments);
        }

    });

    return Terrasoft.SuComponent1Module;

});
