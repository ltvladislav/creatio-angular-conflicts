define("SuComponent1ViewModel", [], function() {
	 
	Ext.define("Terrasoft.configuration.SuComponent1ViewModel", {
		extend: "Terrasoft.BaseViewModel",
		alternateClassName: "Terrasoft.SuComponent1ViewModel",

		Ext: null,
		Terrasoft: null,
		sandbox: null,
		
	});

	return Terrasoft.SuComponent1ViewModel;
});