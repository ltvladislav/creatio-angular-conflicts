(function() {
    require.config({
        paths: {
            "SuComponent1": Terrasoft.getFileContentUrl("_SalesUp_AngularComponentsDemo", "src/js/su-component-1.js"),
            "SuComponent1Css": Terrasoft.getFileContentUrl("_SalesUp_AngularComponentsDemo", "src/css/su-component-2.css"),
            "SuComponent2": Terrasoft.getFileContentUrl("_SalesUp_AngularComponentsDemo", "src/js/su-component-2.js"),
            "SuComponent2Css": Terrasoft.getFileContentUrl("_SalesUp_AngularComponentsDemo", "src/css/su-component-2.css"),
        }
    });
})();