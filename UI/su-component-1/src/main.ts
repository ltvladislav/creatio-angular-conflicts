import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SuComponent1Module } from './su-component-1/su-component-1.module';

platformBrowserDynamic().bootstrapModule(SuComponent1Module)
  .catch(err => console.error(err));