import { NgModule, DoBootstrap, Injector, ApplicationRef } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
import { createCustomElement } from "@angular/elements";

import { SuComponent1Component } from './su-component-1.component';

@NgModule({
	declarations: [
		SuComponent1Component
	],
	imports: [
		BrowserModule
	],
	providers: [],
	entryComponents: [
		SuComponent1Component
	]
})
export class SuComponent1Module implements DoBootstrap {
    constructor(private _injector: Injector) { }
    ngDoBootstrap(appRef: ApplicationRef): void {
		const el: any = createCustomElement(SuComponent1Component, { injector: this._injector });
		if (!customElements.get('su-component-1')) {
			customElements.define('su-component-1', el);
		}        
    }
}