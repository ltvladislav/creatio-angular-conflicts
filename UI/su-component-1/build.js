const fs = require('fs-extra');
const concat = require('concat');
 
(async function build() {
   const files = [
      './dist/su-component-1/runtime-es2015.js',
      './dist/su-component-1/polyfills-es2015.js',
      './dist/su-component-1/main-es2015.js'
   ];

   await fs.ensureDir('component');
   await concat(files, './component/su-component-1.js');
   fs.copyFile('./dist/su-component-1/styles.css', './component/su-component-1.css');
})()
	.then(() => console.log("Element success build"))
	.catch((error) => console.error("Element build error", error));