const fs = require('fs-extra');
const concat = require('concat');
 
(async function build() {
   const files = [
      './dist/su-component-2/runtime-es2015.js',
      './dist/su-component-2/polyfills-es2015.js',
      './dist/su-component-2/main-es2015.js'
   ];

   await fs.ensureDir('component');
   await concat(files, './component/su-component-2.js');
   fs.copyFile('./dist/su-component-2/styles.css', './component/su-component-2.css');
})()
	.then(() => console.log("Element success build"))
	.catch((error) => console.error("Element build error", error));