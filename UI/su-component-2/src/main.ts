import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SuComponent2Module } from './su-component-2/su-component-2.module';

platformBrowserDynamic().bootstrapModule(SuComponent2Module)
  .catch(err => console.error(err));