import { NgModule, DoBootstrap, Injector, ApplicationRef } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
import { createCustomElement } from "@angular/elements";

import { SuComponent2Component } from './su-component-2.component';

@NgModule({
	declarations: [
		SuComponent2Component
	],
	imports: [
		BrowserModule
	],
	providers: [],
	entryComponents: [
		SuComponent2Component
	]
})
export class SuComponent2Module implements DoBootstrap {
    constructor(private _injector: Injector) { }
    ngDoBootstrap(appRef: ApplicationRef): void {
		const el: any = createCustomElement(SuComponent2Component, { injector: this._injector });
		if (!customElements.get('su-component-2')) {
			customElements.define('su-component-2', el);
		}        
    }
}